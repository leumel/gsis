package com.glamourstudio.inventoryapp.glamourstudioapp.singletons;

import android.content.Context;
import android.content.SharedPreferences;

import com.glamourstudio.inventoryapp.glamourstudioapp.models.Admin;
import com.google.gson.Gson;

/**
 * Created by lemuel on 6/6/17.
 */
public class AdminAccountSingleton {
    private static AdminAccountSingleton adminAccountSingleton;
    private SharedPreferences sharedPreferences;

    private final String ADMIN_PREF = "admin_pref";
    private String ADMIN_KEY = "admin";


    private AdminAccountSingleton(Context context){
        sharedPreferences = context.getSharedPreferences(ADMIN_PREF, Context.MODE_PRIVATE);
    }

    public static AdminAccountSingleton getAdminAccountSingleton(Context context){
        if (adminAccountSingleton == null){
            adminAccountSingleton = new AdminAccountSingleton(context);
        }
        return adminAccountSingleton;
    }

    public void updateAdminAccount(Admin admin){
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(ADMIN_KEY, new Gson().toJson(admin));
        editor.commit();
    }


    public Admin getAdmin(){
        String adminStringified = sharedPreferences.getString(ADMIN_KEY,"{}");
        return new Gson().fromJson(adminStringified, Admin.class);
    }

}
