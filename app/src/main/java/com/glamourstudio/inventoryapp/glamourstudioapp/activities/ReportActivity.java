package com.glamourstudio.inventoryapp.glamourstudioapp.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.TextView;

import com.glamourstudio.inventoryapp.glamourstudioapp.R;
import com.glamourstudio.inventoryapp.glamourstudioapp.adapters.ReportItemsAdapter;
import com.glamourstudio.inventoryapp.glamourstudioapp.models.DeliveryItem;
import com.glamourstudio.inventoryapp.glamourstudioapp.models.SessionSpecificReportItems;
import com.glamourstudio.inventoryapp.glamourstudioapp.models.response.LastDeliveryRes;
import com.google.gson.Gson;

import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by lemuel on 4/25/17.
 */
public class ReportActivity extends BaseActivity {

    @BindView(R.id.rv_report_items)
    RecyclerView rvReportItems;

    @BindView(R.id.tv_styles_left)
    TextView tvStylesLeft;

    private ReportItemsAdapter reportItemsAdapter;
    private ArrayList<DeliveryItem> deliveryItems = new ArrayList<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initRecyclerView();
        initReportAdapter();
        updateStylesLeft();
    }

    private void updateStylesLeft() {
        tvStylesLeft.setText(reportItemsAdapter.countReported()+" left to scan");
    }

    private void initRecyclerView() {
        rvReportItems.setLayoutManager(new LinearLayoutManager(this));
    }

    @Override
    protected void onResume() {
        super.onResume();
        deliveryItems.clear();
        deliveryItems.addAll(SessionSpecificReportItems.lastDeliveryRes.getDeliveryItems());
        reportItemsAdapter.notifyDataSetChanged();
    }

    private void initReportAdapter() {
        deliveryItems.addAll(SessionSpecificReportItems.lastDeliveryRes.getDeliveryItems());
        reportItemsAdapter = new ReportItemsAdapter(deliveryItems);
        rvReportItems.setAdapter(reportItemsAdapter);
    }

    @Override
    protected int getLayout() {
        return R.layout.activity_report_qty;
    }

    @OnClick(R.id.tv_scan_action)
    public final void openScan() {
        Intent intent = new Intent(this, ScanQtyActivity.class);
        startActivity(intent);
    }
}
