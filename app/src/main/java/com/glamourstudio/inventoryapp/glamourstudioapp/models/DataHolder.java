package com.glamourstudio.inventoryapp.glamourstudioapp.models;

/**
 * Created by lemuel on 6/6/17.
 */
public class DataHolder <K> {
    private K data;
    private boolean success;
    private boolean is_error;
    private String response_message;

    public K getData() {
        return data;
    }

    public void setData(K data) {
        this.data = data;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public boolean is_error() {
        return is_error;
    }

    public void setIs_error(boolean is_error) {
        this.is_error = is_error;
    }

    public String getResponse_message() {
        return response_message;
    }

    public void setResponse_message(String response_message) {
        this.response_message = response_message;
    }
}
