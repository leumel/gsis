package com.glamourstudio.inventoryapp.glamourstudioapp.models.request;

/**
 * Created by lemuel on 6/5/17.
 */
public class UserLoginReq {
    private String username;
    private String password;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
