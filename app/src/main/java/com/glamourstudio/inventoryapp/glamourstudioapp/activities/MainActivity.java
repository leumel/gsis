package com.glamourstudio.inventoryapp.glamourstudioapp.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.CardView;
import android.widget.TextView;

import com.glamourstudio.inventoryapp.glamourstudioapp.R;
import com.glamourstudio.inventoryapp.glamourstudioapp.models.SessionSpecificReportItems;
import com.glamourstudio.inventoryapp.glamourstudioapp.models.response.LastDeliveryRes;
import com.glamourstudio.inventoryapp.glamourstudioapp.network.GlamourCallback;
import com.glamourstudio.inventoryapp.glamourstudioapp.network.GlamourClient;
import com.google.gson.Gson;

import butterknife.BindView;
import butterknife.OnClick;

public class MainActivity extends BaseActivity {

    enum STOCK_STATE {
        PENDING_REPORT, RECEIVED, FAILED, FETCHING
    }

    @BindView(R.id.tv_stock_status)
    TextView tvStockStatus;

    @BindView(R.id.cv_report_qty)
    CardView cvReportQty;

    @BindView(R.id.cv_receive_delivery)
    CardView cvReceive;

    @BindView(R.id.sfl_main)
    SwipeRefreshLayout sflMain;

    private STOCK_STATE stockState = STOCK_STATE.FETCHING;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        loadLastDeliver();
        listenToPull();
    }

    private void listenToPull() {
        sflMain.setEnabled(false);
        sflMain.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadLastDeliver();
            }
        });
    }

    @Override
    protected int getLayout() {
        return R.layout.activity_main;
    }

    private void loadLastDeliver() {
        GlamourClient.getGlamourClient().getMissionService().getLastDelivery().enqueue(new GlamourCallback<LastDeliveryRes>() {
            @Override
            public void success(LastDeliveryRes data, String message) {
                tvStockStatus.setTextColor(getResources().getColor(android.R.color.black));
                if (data.getDeliveryItems().size() != 0) {
                    stockState = STOCK_STATE.PENDING_REPORT;
                } else {
                    stockState = STOCK_STATE.RECEIVED;
                }
            }

            @Override
            public void failed(String message) {
                tvStockStatus.setTextColor(getResources().getColor(android.R.color.holo_red_dark));
                stockState = STOCK_STATE.FAILED;
            }

            @Override
            public void preCommon(boolean success) {
                sflMain.setRefreshing(false);
                dismissProgressDialog();
            }

            @Override
            protected void postCommon(LastDeliveryRes data, boolean success) {
                super.postCommon(data, success);
                if (stockState == STOCK_STATE.PENDING_REPORT){
                    openReportActivity(data);
                }else {
                    showPopUp("Failed", stockState == STOCK_STATE.RECEIVED ? "No delivery is received yet." : "Request failed try again.");
                }
            }
        });
    }

    @OnClick(R.id.cv_receive_delivery)
    public final void receiveDelivery() {
        if (stockState == STOCK_STATE.PENDING_REPORT) {
            showProgressDialog("Loading", "Receiving delivery..");
            GlamourClient.getGlamourClient().getMissionService().receiveDelivery().enqueue(new GlamourCallback<LastDeliveryRes>() {
                @Override
                public void success(LastDeliveryRes data, String message) {
                    dismissProgressDialog();
                }

                @Override
                public void failed(String message) {
                    dismissProgressDialog();
                    showPopUp("Failed", message);
                }
            });
        }
    }

    @OnClick(R.id.cv_report_qty)
    public final void openReport() {
        loadLastDeliver();
    }

    private void openReportActivity(LastDeliveryRes deliveryItems){
        SessionSpecificReportItems.lastDeliveryRes = deliveryItems;

        Intent intent = new Intent(this, ReportActivity.class);

        startActivity(intent);
    }

    @OnClick(R.id.cv_scan_sale)
    public final void openScanSale() {
        startActivity(new Intent(this, ScanSaleActivity.class));
    }

    @OnClick(R.id.cv_scan_refund)
    public final void openRefund() {
        startActivity(new Intent(this, ScanRefundActivity.class));
    }

    @OnClick(R.id.cv_scan_replace)
    public final void openReplace() {
        startActivity(new Intent(this, ScanReplacementActivity.class));
    }

    @OnClick(R.id.cv_scan_transfer)
    public final void openTransfer() {
        startActivity(new Intent(this, ScanTransferActivity.class));
    }
}
