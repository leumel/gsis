package com.glamourstudio.inventoryapp.glamourstudioapp.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.glamourstudio.inventoryapp.glamourstudioapp.R;
import com.glamourstudio.inventoryapp.glamourstudioapp.models.DeliveryItem;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by lemuel on 6/9/17.
 */
public class ReportItemsAdapter extends RecyclerView.Adapter<ReportItemsAdapter.ReportItemsViewHolder> {

    private ArrayList<DeliveryItem> deliveryItems = new ArrayList<>();
    private Context context;

    public ReportItemsAdapter(ArrayList<DeliveryItem> deliveryItems){
        this.deliveryItems = deliveryItems;
    }

    @Override
    public ReportItemsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (context==null){
            context = parent.getContext();
        }
        return new ReportItemsViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_stock, null));
    }

    @Override
    public void onBindViewHolder(final ReportItemsViewHolder holder, int position) {
        final DeliveryItem deliveryItem = deliveryItems.get(position);
        holder.tvItemScanStatus.setText("Unscanned");
        holder.tvItemSKU.setText(deliveryItem.getSku());
        holder.tvReportedQty.setText("-");
        holder.tvExpectedQty.setText("" + deliveryItem.getExpectedQty());
        holder.tvItemSize.setText(deliveryItem.getSize());
        holder.tvItemDescription.setText(deliveryItem.getDescription() );
        holder.tvItemColor.setText(deliveryItem.getColor());

        Glide.with(context).load(deliveryItem.getImageUrl()).into(holder.ivItemImage);

        holder.btnMissing.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deliveryItem.setNumDelivered(0);
                holder.tvReportedQty.setText(deliveryItem.getNumDelivered() + "");
                deliveryItem.setReported(true);
            }
        });
    }

    @Override
    public int getItemCount() {
        return deliveryItems.size();
    }

    class ReportItemsViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_item_scan_status)
        TextView tvItemScanStatus;

        @BindView(R.id.tv_item_sku)
        TextView tvItemSKU;

        @BindView(R.id.tv_item_reported_qty)
        TextView tvReportedQty;

        @BindView(R.id.iv_item_image)
        ImageView ivItemImage;

        @BindView(R.id.tv_item_expected_qty)
        TextView tvExpectedQty;

        @BindView(R.id.tv_item_size)
        TextView tvItemSize;

        @BindView(R.id.tv_item_description)
        TextView tvItemDescription;

        @BindView(R.id.tv_item_color)
        TextView tvItemColor;

        @BindView(R.id.btn_missing)
        Button btnMissing;


        public ReportItemsViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public int countReported() {
        int reportedCount = 0;
        for (DeliveryItem deliveryItem : deliveryItems){
            if(deliveryItem.isReported()){
                reportedCount++;
            }
        }
        return reportedCount;

    }
}
