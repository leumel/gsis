package com.glamourstudio.inventoryapp.glamourstudioapp.activities

import android.app.ProgressDialog
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import butterknife.ButterKnife
import com.glamourstudio.inventoryapp.glamourstudioapp.R

/**
 * Created by lemuel on 4/24/17.
 */
abstract class BaseActivity : AppCompatActivity() {

    protected abstract val layout: Int
    private var popup: View? = null
    private var progressDialog : ProgressDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(layout)
        ButterKnife.bind(this)
    }

    protected fun showPopUp(popupTitle : String, popupText : String) {
        popup = LayoutInflater.from(this).inflate(R.layout.popup_generic, null)

        val tvPopupTitle = popup?.findViewById(R.id.tv_popup_title) as TextView
        val tvPopupText = popup?.findViewById(R.id.tv_popup_text) as TextView
        val tvPopupClose = popup?.findViewById(R.id.tv_popup_close)

        tvPopupTitle.setText(popupTitle)
        tvPopupText.setText(popupText)

        tvPopupClose?.setOnClickListener {
            removePopUp()
        }

        getRootView().addView(popup)
    }

    protected fun showProgressDialog(progressTitle : String, progressText : String) {
        progressDialog = ProgressDialog.show(this, progressTitle, progressText, false)
        progressDialog?.show()
    }

    protected fun dismissProgressDialog(){
        if (progressDialog!=null){
            progressDialog?.dismiss()
        }
    }

    private fun getRootView() : ViewGroup {
        return window.decorView.findViewById(android.R.id.content) as ViewGroup
    }

    private fun removePopUp() {
        getRootView().removeView(popup)
    }
}
