package com.glamourstudio.inventoryapp.glamourstudioapp.network;

import com.glamourstudio.inventoryapp.glamourstudioapp.errors.GenericAPIError;
import com.glamourstudio.inventoryapp.glamourstudioapp.models.DataHolder;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by lemuel on 6/6/17.
 */
public abstract class GlamourCallback<K> implements Callback<DataHolder<K>> {
    public abstract void success(K data, String message);
    public abstract void failed(String message);

    protected void preCommon(boolean success){
        //used for setup. note that this is called before success and failed
    }

    protected void postCommon(K data, boolean success){
        //used for setup. note that this is called after success and failed
    }


    @Override
    public void onResponse(Call<DataHolder<K>> call, Response<DataHolder<K>> response) {
        DataHolder<K> body = response.body();
        if (response.isSuccessful()) {
            if (body == null) {
                preCommon(false);
                onFailure(call, new GenericAPIError("No data received"));
                postCommon(null, false);
            } else if (body.isSuccess()) {
                preCommon(true);
                success(response.body().getData(), response.body().getResponse_message());
                postCommon(response.body().getData(), true);
            } else {
                preCommon(false);
                onFailure(call, new GenericAPIError(body.getResponse_message()));
                postCommon(null, false);
            }
        }

    }

    @Override
    public void onFailure(Call<DataHolder<K>> call, Throwable t) {
        preCommon(false);
        failed(t.getMessage());
        postCommon(null, false);
    }
}
