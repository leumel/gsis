package com.glamourstudio.inventoryapp.glamourstudioapp.network;

import com.glamourstudio.inventoryapp.glamourstudioapp.models.DataHolder;
import com.glamourstudio.inventoryapp.glamourstudioapp.models.request.UserLoginReq;
import com.glamourstudio.inventoryapp.glamourstudioapp.models.response.LastDeliveryRes;
import com.glamourstudio.inventoryapp.glamourstudioapp.models.response.UserLoginRes;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * Created by lemuel on 6/5/17.
 */
public interface GlamourService {

    @POST("api/user/login")
    Call<DataHolder<UserLoginRes>> loginUser(@Body UserLoginReq userLoginReq);

    @POST("/api/delivery/receive/get-last")
    Call<DataHolder<LastDeliveryRes>> getLastDelivery();

    @POST("/api/delivery/receive")
    Call<DataHolder<LastDeliveryRes>> receiveDelivery();

}
