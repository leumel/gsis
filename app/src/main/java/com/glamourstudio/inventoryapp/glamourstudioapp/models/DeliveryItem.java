package com.glamourstudio.inventoryapp.glamourstudioapp.models;

/**
 * Created by lemuel on 6/9/17.
 */
public class DeliveryItem {
    private int id;
    private String sku;
    private String color;
    private String size;
    private String imageUrl;
    private int startingQty;
    private int expectedQty;
    private int numDelivered;
    private boolean reported;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    private String description;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public int getStartingQty() {
        return startingQty;
    }

    public void setStartingQty(int startingQty) {
        this.startingQty = startingQty;
    }

    public int getExpectedQty() {
        return expectedQty;
    }

    public void setExpectedQty(int expectedQty) {
        this.expectedQty = expectedQty;
    }

    public int getNumDelivered() {
        return numDelivered;
    }

    public void setNumDelivered(int numDelivered) {
        this.numDelivered = numDelivered;
    }

    public boolean isReported() {
        return reported;
    }

    public void setReported(boolean reported) {
        this.reported = reported;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }
}
