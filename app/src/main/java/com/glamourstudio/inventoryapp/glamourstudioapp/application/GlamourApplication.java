package com.glamourstudio.inventoryapp.glamourstudioapp.application;

import android.app.Application;

import com.glamourstudio.inventoryapp.glamourstudioapp.network.GlamourClient;
import com.glamourstudio.inventoryapp.glamourstudioapp.singletons.AdminAccountSingleton;

/**
 * Created by lemuel on 6/6/17.
 */
public class GlamourApplication extends Application{

    @Override
    public void onCreate() {
        super.onCreate();
        initSharedPrefs();
        initRestClient();
    }

    private void initRestClient() {
        GlamourClient.getGlamourClient().init(getApplicationContext());
    }

    private void initSharedPrefs() {
        AdminAccountSingleton.getAdminAccountSingleton(getApplicationContext());
    }
}
