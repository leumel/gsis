package com.glamourstudio.inventoryapp.glamourstudioapp.errors;

/**
 * Created by lemuel on 6/6/17.
 */
public class GenericAPIError extends Throwable {

    private String message;

    public GenericAPIError(String message) {
        this.message = message;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
