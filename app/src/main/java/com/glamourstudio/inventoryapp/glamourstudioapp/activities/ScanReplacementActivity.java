package com.glamourstudio.inventoryapp.glamourstudioapp.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;

import com.glamourstudio.inventoryapp.glamourstudioapp.R;
import com.glamourstudio.inventoryapp.glamourstudioapp.fragments.ScanFragment;

/**
 * Created by lemuel on 4/25/17.
 */
public class ScanReplacementActivity extends BaseActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addScanFragment();
    }

    private void addScanFragment() {
        ScanFragment scanFragment = new ScanFragment();
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.add(R.id.fl_scanfragment_holder, scanFragment);
        ft.commit();
    }

    @Override
    protected int getLayout() {
        return R.layout.activity_scan_replace;
    }
}
