package com.glamourstudio.inventoryapp.glamourstudioapp.models.response;

import com.glamourstudio.inventoryapp.glamourstudioapp.models.Admin;

/**
 * Created by lemuel on 6/6/17.
 */
public class UserLoginRes {
    private Admin admin;

    public Admin getAdmin() {
        return admin;
    }

    public void setAdmin(Admin admin) {
        this.admin = admin;
    }
}
