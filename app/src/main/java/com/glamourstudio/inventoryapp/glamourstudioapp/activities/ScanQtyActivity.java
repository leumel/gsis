package com.glamourstudio.inventoryapp.glamourstudioapp.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.glamourstudio.inventoryapp.glamourstudioapp.R;
import com.glamourstudio.inventoryapp.glamourstudioapp.fragments.ScanFragment;
import com.glamourstudio.inventoryapp.glamourstudioapp.models.DeliveryItem;
import com.glamourstudio.inventoryapp.glamourstudioapp.models.SessionSpecificReportItems;
import com.glamourstudio.inventoryapp.glamourstudioapp.models.response.LastDeliveryRes;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;

/**
 * Created by lemuel on 4/25/17.
 */
public class ScanQtyActivity extends BaseActivity implements ScanFragment.ScanListener {

    @BindView(R.id.rl_item_holder)
    RelativeLayout itemHolder;

    @BindView(R.id.iv_item_prev)
    ImageView ivItemPrev;

    @BindView(R.id.tv_item_sku)
    TextView tvItemSku;

    @BindView(R.id.tv_item_size)
    TextView tvItemSize;

    @BindView(R.id.tv_item_description)
    TextView tvItemDesc;

    @BindView(R.id.tv_item_expected_qty)
    TextView tvItemExpectedQty;

    @BindView(R.id.tv_item_reported_qty)
    TextView tvItemReportedQty;

    @BindView(R.id.btn_save)
    Button btnSave;

    @BindView(R.id.btn_cancel)
    Button tvBtnCancel;

    public final static String REPORT_ITEMS = "report_items";
    private HashMap<String, DeliveryItem> deliveryItems = new HashMap<>();
    private ScanFragment scanFragment;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getExtra();
        addScanFragment();

    }

    private void getExtra() {
        LastDeliveryRes reportItems = SessionSpecificReportItems.lastDeliveryRes;
        for (DeliveryItem deliveryItem : reportItems.getDeliveryItems()){
            deliveryItems.put(deliveryItem.getSku(), deliveryItem);
        }
    }

    private void addScanFragment() {
        scanFragment = new ScanFragment();
        FragmentTransaction ft  = getSupportFragmentManager().beginTransaction();
        ft.add(R.id.fl_scanfragment_holder, scanFragment);
        ft.commit();

        scanFragment.setScanListener(this);
    }

    @Override
    protected int getLayout() {
        return R.layout.activity_scan_report_qty;
    }

    @Override
    public void onScan(String contents) {
        itemHolder.setVisibility(View.VISIBLE);
        lookupItem(contents);
    }

    private void lookupItem(String contents) {
        inflateView(deliveryItems.get(contents));
    }

    private void inflateView(DeliveryItem deliveryItem){
        if (deliveryItem == null){
            showPopUp("Error", "No item found with matching sku");
            scanFragment.resumeCamera();
            itemHolder.setVisibility(View.GONE);
        }else {
            Glide.with(this).load(deliveryItem.getImageUrl()).into(ivItemPrev);
            tvItemSku.setText(deliveryItem.getSku());
            tvItemSize.setText(deliveryItem.getSize());
            tvItemDesc.setText(deliveryItem.getDescription());
            tvItemReportedQty.setText("0");
            tvItemExpectedQty.setText(deliveryItem.getExpectedQty());
            View.OnClickListener actionsClickListener = new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    switch (view.getId()){
                        case R.id.btn_save:
                            itemHolder.setVisibility(View.GONE);
                            return;
                        case R.id.btn_cancel:
                            itemHolder.setVisibility(View.GONE);
                            return;
                    }

                    scanFragment.resumeCamera();

                }
            };

            btnSave.setOnClickListener(actionsClickListener);
            tvBtnCancel.setOnClickListener(actionsClickListener);
        }
    }
}
