package com.glamourstudio.inventoryapp.glamourstudioapp.models;

/**
 * Created by lemuel on 6/6/17.
 */
public class Branch {
    private int id;
    private String name;
    private String address;
    private double latitude;
    private double longitude;
    private String date_created;
    private String date_last_modified;
    private String date_deleted;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getDate_created() {
        return date_created;
    }

    public void setDate_created(String date_created) {
        this.date_created = date_created;
    }

    public String getDate_last_modified() {
        return date_last_modified;
    }

    public void setDate_last_modified(String date_last_modified) {
        this.date_last_modified = date_last_modified;
    }

    public String getDate_deleted() {
        return date_deleted;
    }

    public void setDate_deleted(String date_deleted) {
        this.date_deleted = date_deleted;
    }
}
