package com.glamourstudio.inventoryapp.glamourstudioapp.models.response;

import com.glamourstudio.inventoryapp.glamourstudioapp.models.DeliveryItem;

import java.util.List;

/**
 * Created by lemuel on 6/9/17.
 */
public class LastDeliveryRes {
    List<DeliveryItem> deliveryItems;

    public List<DeliveryItem> getDeliveryItems() {
        return deliveryItems;
    }

    public void setDeliveryItems(List<DeliveryItem> deliveryItems) {
        this.deliveryItems = deliveryItems;
    }
}
