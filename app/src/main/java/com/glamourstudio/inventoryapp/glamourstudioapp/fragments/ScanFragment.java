package com.glamourstudio.inventoryapp.glamourstudioapp.fragments;

/**
 * Created by lemuel on 4/25/17.
 */

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import me.dm7.barcodescanner.zbar.Result;
import me.dm7.barcodescanner.zbar.ZBarScannerView;

/**
 * Created by lemuel on 1/11/17.
 */
public class ScanFragment extends Fragment implements ZBarScannerView.ResultHandler {
    @Override
    public void handleResult(Result result) {
        // Do something with the result here
        Log.v("lemz", result.getContents()); // Prints scan results
        Log.v("lemz", result.getBarcodeFormat().toString()); // Prints the scan format (qrcode, pdf417 etc.)

        // If you would like to resume scanning, call this method below:
        if (scanListener!=null){
            scanListener.onScan(result.getContents());
        }
    }

    public interface ScanListener{
        void onScan(String contents);
    }

    private ScanListener scanListener;

    private ZBarScannerView mScannerView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mScannerView = new ZBarScannerView(getContext());
        mScannerView.setAutoFocus(true);
        mScannerView.setFlash(true);
        return mScannerView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
        mScannerView.setResultHandler(this); // Register ourselves as a handler for scan results.
        mScannerView.startCamera();          // Start camera on resume
    }

    @Override
    public void onPause() {
        super.onPause();
        mScannerView.stopCamera();           // Stop camera on pause
    }

    public void resumeCamera(){
        mScannerView.resumeCameraPreview(ScanFragment.this);
    }

    public ScanListener getScanListener() {
        return scanListener;
    }

    public void setScanListener(ScanListener scanListener) {
        this.scanListener = scanListener;
    }

}

