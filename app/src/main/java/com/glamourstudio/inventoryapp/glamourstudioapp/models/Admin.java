package com.glamourstudio.inventoryapp.glamourstudioapp.models;

/**
 * Created by lemuel on 6/6/17.
 */
public class Admin {
    private int id;
    private String type;
    private String name;
    private String username;
    private String remember_token;
    private String date_created;
    private String date_last_modified;
    private String date_deleted;
    private String date_last_logged_in;
    private Branch branch;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getRemember_token() {
        return remember_token;
    }

    public void setRemember_token(String remember_token) {
        this.remember_token = remember_token;
    }

    public String getDate_created() {
        return date_created;
    }

    public void setDate_created(String date_created) {
        this.date_created = date_created;
    }

    public String getDate_last_modified() {
        return date_last_modified;
    }

    public void setDate_last_modified(String date_last_modified) {
        this.date_last_modified = date_last_modified;
    }

    public String getDate_deleted() {
        return date_deleted;
    }

    public void setDate_deleted(String date_deleted) {
        this.date_deleted = date_deleted;
    }

    public String getDate_last_logged_in() {
        return date_last_logged_in;
    }

    public void setDate_last_logged_in(String date_last_logged_in) {
        this.date_last_logged_in = date_last_logged_in;
    }

    public Branch getBranch() {
        return branch;
    }

    public void setBranch(Branch branch) {
        this.branch = branch;
    }
}
