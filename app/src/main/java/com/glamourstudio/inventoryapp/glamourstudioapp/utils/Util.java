package com.glamourstudio.inventoryapp.glamourstudioapp.utils;

import android.content.Context;
import android.content.Intent;

/**
 * Created by lemuel on 6/6/17.
 */
public class Util {

    public static final void openActivityClearStack(Context context, Class newActivity){
        Intent intent = new Intent(context, newActivity);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }
}
