package com.glamourstudio.inventoryapp.glamourstudioapp.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import com.glamourstudio.inventoryapp.glamourstudioapp.R;
import com.glamourstudio.inventoryapp.glamourstudioapp.models.Admin;
import com.glamourstudio.inventoryapp.glamourstudioapp.models.request.UserLoginReq;
import com.glamourstudio.inventoryapp.glamourstudioapp.models.response.UserLoginRes;
import com.glamourstudio.inventoryapp.glamourstudioapp.network.GlamourCallback;
import com.glamourstudio.inventoryapp.glamourstudioapp.network.GlamourClient;
import com.glamourstudio.inventoryapp.glamourstudioapp.network.GlamourService;
import com.glamourstudio.inventoryapp.glamourstudioapp.singletons.AdminAccountSingleton;
import com.google.gson.Gson;

import org.jetbrains.annotations.NotNull;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by lemuel on 6/6/17.
 */
public class LoginActivity extends BaseActivity {

    @BindView(R.id.et_username)
    EditText etUname;
    @BindView(R.id.et_password)
    EditText etPassword;

    private AdminAccountSingleton adminAccountSingleton;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        adminAccountSingleton = AdminAccountSingleton.getAdminAccountSingleton(this);
//        if (adminExist()) {
//            openMainActivity();
//        }
    }

    private boolean adminExist() {
        Admin admin = adminAccountSingleton.getAdmin();
        if (admin == null) {
            return false;
        }
        return admin.getId() != 0;
    }

    @Override
    protected int getLayout() {
        return R.layout.activity_login;
    }

    @OnClick(R.id.btn_login)
    public final void loginUser() {
        showProgressDialog("Loading", "Logging you in...");

        GlamourClient.getGlamourClient().getMissionService().loginUser(getUserLoginReq()).enqueue(new GlamourCallback<UserLoginRes>() {
            @Override
            public void success(UserLoginRes data, String message) {
                adminAccountSingleton.updateAdminAccount(data.getAdmin());
                openMainActivity();
                dismissProgressDialog();
            }

            @Override
            public void failed(String message) {
                dismissProgressDialog();
                showPopUp("Failed", message);
            }
        });
    }


    private void openMainActivity() {
        startActivity(new Intent(this, MainActivity.class));
        finish();
    }

    public UserLoginReq getUserLoginReq() {
        UserLoginReq userLoginReq = new UserLoginReq();
        userLoginReq.setUsername(etUname.getText().toString());
        userLoginReq.setPassword(etPassword.getText().toString());
        return userLoginReq;
    }
}
